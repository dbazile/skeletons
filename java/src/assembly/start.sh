#!/bin/bash -e

cd "$(dirname ""$0"")"

java \
	-Dconfig_path='conf/config.properties' \
	-cp "$PWD/conf/" \
	-jar main.jar \
	"$@"
