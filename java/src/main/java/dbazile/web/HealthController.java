package dbazile.web;

import io.javalin.http.Context;

import java.time.Duration;
import java.time.Instant;

import static dbazile.web.ResponseUtils.orderedMap;
import static dbazile.web.ResponseUtils.wrapped;

public class HealthController {
    private static final Instant START = Instant.now();

    public void health(Context context) {
        final var uptime = Duration.between(START, Instant.now()).toSeconds();

        context.json(wrapped("health", orderedMap("system", "myproject", "uptime", uptime)));
    }
}
