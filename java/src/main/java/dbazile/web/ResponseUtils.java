package dbazile.web;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class ResponseUtils {

    public static Map<String, Object> orderedMap(String key, Object value, Object... kvPairs) {
        if (kvPairs.length % 2 != 0) {
            throw new IllegalArgumentException("kvPairs must be an even number of elements");
        }

        final var map = new LinkedHashMap<String, Object>();

        // Add first kv pair
        map.put(key, value);

        // Iterate over rest of kv pairs
        for (int i = 0; i < kvPairs.length; i += 2) {
            map.put(
                String.valueOf(Objects.requireNonNull(kvPairs[i], "key at index " + i + " cannot be null")),
                kvPairs[i + 1]
            );
        }

        return map;
    }

    public static Map<String, Object> wrapped(String key, Object value, Object... kvPairs) {
        final var map = orderedMap(key, value, kvPairs);
        map.put("timestamp", Instant.now().toString());
        return map;
    }
}
