package dbazile.web;

import dbazile.config.Config;
import io.javalin.http.Context;

import java.util.Optional;

import static dbazile.web.ResponseUtils.wrapped;

public class GreetingController {
    private final Config config;

    public GreetingController(Config config) {
        this.config = config;
    }

    public void greet(Context context) {
        final var name = Optional.ofNullable(context.queryParam("name")).orElse("World");
        context.json(wrapped("greeting", new Greeting("Hi there " + name, config.getColor())));
    }

    static class Greeting {
        private final String color;
        private final String data;

        private Greeting() {
            this.color = null;
            this.data = null;
        }

        public Greeting(String data, String color) {
            this.color = color;
            this.data = data;
        }

        public String getColor() {
            return color;
        }

        public String getData() {
            return data;
        }
    }
}
