package dbazile.web;

import io.javalin.http.Context;
import io.javalin.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import static dbazile.web.ResponseUtils.wrapped;

public final class ErrorHandlers {
    private static final Logger LOG = LoggerFactory.getLogger(ErrorHandlers.class);

    public static void _404(Context context) {
        errorResponse(context, HttpStatus.NOT_FOUND, null);
    }

    public static void _500(Exception e, Context context) {
        LOG.error("{}: {}", ErrorHandlers.class.getSimpleName(), e.getMessage(), e);
        errorResponse(context, HttpStatus.INTERNAL_SERVER_ERROR, null);
    }

    public static void errorResponse(Context context, int status, String message) {
        errorResponse(context, HttpStatus.forStatus(status), message);
    }

    public static void errorResponse(Context context, HttpStatus status, String message) {
        try {
            context
                .status(status)
                .json(wrapped("error", Optional.ofNullable(message).orElse(status.toString())));
        }
        catch (Exception e) {
            LOG.error("error generating response", e);
            context.status(status).result("oh no");
        }
    }
}
