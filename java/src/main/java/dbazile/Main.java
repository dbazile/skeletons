package dbazile;

import dbazile.config.Config;
import dbazile.web.ErrorHandlers;
import dbazile.web.GreetingController;
import dbazile.web.HealthController;
import io.javalin.Javalin;
import io.javalin.community.ssl.SSLPlugin;
import io.javalin.config.JavalinConfig;
import io.javalin.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

public final class Main {
    private static final Logger ACCESS_LOG = LoggerFactory.getLogger("access");
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        final var config = Config.getInstance();

        LOG.info("start");

        Javalin.create(createConfigurer(config))

                // Routes
                .get("/health", new HealthController()::health)
                .get("/", new GreetingController(config)::greet)

                // Error handling
                .error(HttpStatus.NOT_FOUND, ErrorHandlers::_404)
                .exception(Exception.class, ErrorHandlers::_500)

                // Listen
                .start(config.getBindHost(), config.getBindPort());

        LOG.info("started");
    }

    private static Consumer<JavalinConfig> createConfigurer(Config config) {
        return cfg -> {
            cfg.showJavalinBanner = false;

            // TLS
            if (config.getTlsEnabled()) {
                cfg.plugins.register(new SSLPlugin(sslConfig -> {
                    sslConfig.insecure = false;
                    sslConfig.securePort = config.getBindPort();
                    sslConfig.keystoreFromPath(config.getTlsCertPath(), config.getTlsCertPassword());
                }));
            }

            // Request Logging
            cfg.requestLogger.http((context, ms) -> {
                ACCESS_LOG.info("{} \"{}\" {} \"{}\"",
                        context.method(),
                        context.url(),
                        context.ip(),
                        context.userAgent());
            });
        };
    }
}
