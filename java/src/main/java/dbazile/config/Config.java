package dbazile.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public final class Config {
    private static final Logger LOG = LoggerFactory.getLogger(Config.class);

    private static final String PROPERTY_OVERRIDE_PATH = "config_path";
    private static final String RESOURCE_PATH = "config.properties";

    private static final String KEY_COLOR = "myproject.color";
    private static final String KEY_HOST = "myproject.host";
    private static final String KEY_PORT = "myproject.port";
    private static final String KEY_TLS_CERT_PASSWORD = "myproject.tls.certPassword";
    private static final String KEY_TLS_CERT_PATH = "myproject.tls.certPath";
    private static final String KEY_TLS_ENABLED = "myproject.tls.enabled";

    private static final Config INSTANCE = new Config();

    private final String bindHost;
    private final int bindPort;
    private final String color;
    private final String tlsCertPassword;
    private final String tlsCertPath;
    private final boolean tlsEnabled;

    private Config() {
        final Properties props = loadProperties();

        this.bindHost = readString(props, KEY_HOST);
        this.bindPort = readInt(props, KEY_PORT);
        this.color = readString(props, KEY_COLOR);
        this.tlsEnabled = readBool(props, KEY_TLS_ENABLED);
        this.tlsCertPath = this.tlsEnabled ? readString(props, KEY_TLS_CERT_PATH) : null;
        this.tlsCertPassword = this.tlsEnabled ? readString(props, KEY_TLS_CERT_PASSWORD) : null;
    }

    public static Config getInstance() throws ConfigException {
        return INSTANCE;
    }

    public String getBindHost() {
        return bindHost;
    }

    public int getBindPort() {
        return bindPort;
    }

    public String getTlsCertPassword() {
        return tlsCertPassword;
    }

    public String getTlsCertPath() {
        return tlsCertPath;
    }

    public String getColor() {
        return color;
    }

    public boolean getTlsEnabled() {
        return tlsEnabled;
    }

    private static Properties loadProperties() {
        final Properties properties = new Properties();

        LOG.info("read from classpath '{}'", RESOURCE_PATH);

        // Read configuration defaults
        try (final InputStream stream = Config.class.getClassLoader().getResourceAsStream(RESOURCE_PATH)) {
            properties.load(stream);
        }
        catch (IOException e) {
            throw new ConfigException("could not read resource '" + RESOURCE_PATH + "'", e);
        }

        // Apply configuration overrides
        final String overridePath = System.getProperty(PROPERTY_OVERRIDE_PATH);
        if (overridePath != null) {
            final Properties overrides = new Properties();
            final Path path = Paths.get(overridePath);

            LOG.info("read overrides from '{}'", path.toAbsolutePath());

            try (final InputStream stream = Files.newInputStream(path)) {
                overrides.load(stream);
            }
            catch (NoSuchFileException e) {
                LOG.warn("config override file '{}' not found", overridePath);
            }
            catch (IOException e) {
                throw new ConfigException("could not read '" + path + "': ", e);
            }

            properties.putAll(overrides);
        }

        return properties;
    }

    private static boolean readBool(Properties prop, String key) {
        return Boolean.parseBoolean(readString(prop, key));
    }

    private static int readInt(Properties props, String key) {
        try {
            return Integer.parseInt(readString(props, key));
        }
        catch (NumberFormatException e) {
            throw new ConfigException("invalid integer at property '" + key + "': " + e.getMessage(), e);
        }
    }

    private static String readString(Properties props, String key) {
        final String value = System.getProperty(key, props.getProperty(key, "")).trim();

        if (value.isEmpty()) {
            throw new ConfigException("missing value for '" + key + "'");
        }

        return value;
    }

    public static final class ConfigException extends RuntimeException {
        ConfigException(String message) {
            super(message);
        }

        ConfigException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
