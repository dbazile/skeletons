# myproject

## Running

    mvn compile exec:java
    

## Building

    mvn clean package

Running the built artifact:

    java -jar target/myproject-x.x.x-runnable.jar


### Configuration

Point to a config overrides file:

	java -Dconfig_path=/path/to/config \
         -jar target/myproject-x.x.x-runnable.jar

Override one or more individual properties:

	java -Dmyproject.greeting='how do you do' \
         -jar target/myproject-x.x.x-runnable.jar


## Testing

    mvn test
