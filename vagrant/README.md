# vagrant

Working configuration for Vagrant via libvirt on Fedora 35 with no
extra dependencies.


## Versions

- Fedora 35
- libvirt 7.6.0
- vagrant 2.2.16


## Setup

```bash
sudo dnf install @virtualization @vagrant
sudo usermod -aG libvirt "$USER"
sudo systemctl enable --now libvirtd
newgrp libvirt
vagrant up
```


## Notes

- To see VMs, run _Virtual Machine Manager_ (package `virt-manager`).
- If a box defines shared folders, the type will default to NFS, which
  prompts for a root password. To find what needs to be
  overridden/disabled, run vagrant in verbose mode, `VAGRANT_LOG=info
  vagrant up`, and look for `synced_folders` in the output.
- References:
    - [Fedora Docs: Getting Started with Virtualization (libvirt)](https://docs.fedoraproject.org/en-US/quick-docs/getting-started-with-virtualization/) - libvirt install and config documentation.
    - [Fedora Developer: Vagrant with libvirt support installation](https://developer.fedoraproject.org/tools/vagrant/vagrant-libvirt.html)
    - [vagrant-libvirt options](https://github.com/vagrant-libvirt/vagrant-libvirt#domain-specific-options)
    - [Get rid of password prompt for vagrant commands on libvirt](https://lalatendu.org/2016/02/24/get-rid-of-password-prompt-for-vagrant-commands-on-libvirt/) - Not sure if this one is still valid; didn't seem to do anything.
