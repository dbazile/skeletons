#!/bin/bash -e

source "$(dirname $0)/_common.sh"
################################################################################


checkpoint 'run server'

cd src

gunicorn \
	--reload \
	myproject:app
