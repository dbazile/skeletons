#!/bin/bash -e

source "$(dirname $0)/_common.sh"
################################################################################


checkpoint 'typechecking with mypy'

mypy "$@" -- src/myproject/ src/tests/
