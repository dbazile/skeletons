#!/bin/bash -e

cd $(dirname $(dirname $0))
################################################################################


checkpoint() { printf '\n\033[43;30m %s \033[0m\n\n' "$*"; }

################################################################################

if [ ! -d venv ]; then
    printf "It looks like your development environment is not set up.\n\nSet it up now?\n"
    read -p "(y/N) " -r

    if [[ ! "$REPLY" =~ ^[Yy] ]]; then
        printf '\n\e[31m%s\e[0m\n' "Exiting..."
        exit 1
    fi

    checkpoint "Creating $(python3 --version) environment"

    rm -rf venv
    python3 -m venv venv
    source venv/bin/activate

    checkpoint 'Installing Python dependencies'

	pip install -r requirements.txt -r requirements-dev.txt
fi

################################################################################


source venv/bin/activate
