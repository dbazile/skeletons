#!/bin/bash -e

source "$(dirname $0)/_common.sh"
################################################################################


ARCHIVE_FILENAME='myproject.tgz'

################################################################################

checkpoint 'create single-file executable'

pyinstaller \
	--onefile \
	--name 'myproject' \
	--hidden-import 'gunicorn.glogging' \
	--hidden-import 'gunicorn.workers.sync' \
	src/gunicorn_wrapper.py

################################################################################

checkpoint "build archive '$ARCHIVE_FILENAME'"

rm -f "$ARCHIVE_FILENAME"
tar zcvf "$ARCHIVE_FILENAME" -- dist/
