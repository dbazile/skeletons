#!/bin/bash -e

source "$(dirname $0)/_common.sh"
################################################################################


checkpoint 'clean up'

rm -rfv \
    *.spec \
    .mypy_cache \
    build \
    dist \
    myproject.tgz \
    src/.coverage \
    src/myproject/__pycache__ \
    src/tests/__pycache__
