#!/bin/bash -e

source "$(dirname $0)/_common.sh"
################################################################################


./scripts/mypy.sh

checkpoint 'Running tests with coverage'

cd src

coverage run --source=myproject -m unittest
coverage report
