from unittest import TestCase

from myproject.foo import lorem_ipsum


class FooTest(TestCase):

    def test_something(self):
        self.assertEqual('lorem ipsum dolor sit amet', lorem_ipsum())

    def test_something_fail(self):
        self.assertEqual(True, lorem_ipsum())
