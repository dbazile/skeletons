# myproject


## Running

```bash
./scripts/develop.sh
```


## Testing

```bash
./scripts/test.sh
```


## Packaging for Deployment

```bash
# note: building on non-Alpine to run on Alpine may not work
#       https://github.com/gliderlabs/docker-alpine/issues/48
./scripts/package.sh
```
